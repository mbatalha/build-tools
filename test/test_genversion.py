from __future__ import absolute_import
import genversion
import pytest

def test_removePrefix():
    assert genversion.removePrefix("abc", "a") == "bc"
    assert genversion.removePrefix("abc", "c") == "abc"

def test_SoftwareVersion():
    version = genversion.SoftwareVersion(0, 0, 1, 3)
    assert version.toString() == "0.0.1.3"

    version = genversion.SoftwareVersion(1, 3, 4)
    assert version.toString() == "1.3.4"

def test_GitDescribe_parsing():
    describe = genversion.GitDescribe("0.0.6-dirty")
    assert describe.isDirty()
    assert describe.getCommitHash() is None
    assert describe.getNumberOfCommits() is None
    assert describe.getVersionTriplet() == (0, 0, 6)
    assert describe.getMiniPatch() == "dirty"

    describe = genversion.GitDescribe("0.0.6")
    assert not describe.isDirty()
    assert describe.getCommitHash() is None
    assert describe.getNumberOfCommits() is None
    assert describe.getVersionTriplet() == (0, 0, 6)
    assert describe.getMiniPatch() == None

    describe = genversion.GitDescribe("R_0_6_6-19-ga5f6a43-dirty")
    assert describe.getCommitHash() == "a5f6a43"
    assert describe.isDirty()
    assert describe.getNumberOfCommits() == 19
    assert describe.getVersionTriplet() == (0, 6, 6)
    assert describe.getMiniPatch() == "19.a5f6a43.dirty"

    describe = genversion.GitDescribe("R_0_6_6-1-gggg-dirty")
    assert describe.getCommitHash() == "ggg"
    assert describe.isDirty()
    assert describe.getNumberOfCommits() == 1
    assert describe.getVersionTriplet() == (0, 6, 6)
    assert describe.getMiniPatch() == "1.ggg.dirty"

    describe = genversion.GitDescribe("0_6_6-1337-ga5f6a43")
    assert describe.getCommitHash() == "a5f6a43"
    assert not describe.isDirty()
    assert describe.getNumberOfCommits() == 1337
    assert describe.getVersionTriplet() == (0, 6, 6)
    assert describe.getMiniPatch() == "1337.a5f6a43"

    describe = genversion.GitDescribe("v0_6-999-gaaa")
    assert describe.getCommitHash() == "aaa"
    assert not describe.isDirty()
    assert describe.getNumberOfCommits() == 999
    assert describe.getVersionTriplet() == (0, 6, 0)
    assert describe.getMiniPatch() == "999.aaa"

    with pytest.raises(Exception):
        describe = genversion.GitDescribe("v0-999-gaaa")

def test_applyTemplate():
    replacements = [
        ["@STUFF@", "awesome_stuff"],
        ["@ASDF@", 3]
    ]

    applied = genversion.applyTemplate("123 @STUFF@ 234 @ASDF@", replacements)
    assert applied == "123 awesome_stuff 234 3"
