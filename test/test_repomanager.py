from __future__ import absolute_import
repomanager = __import__('repo-manager')
import pytest

def test_PackageNameParser():
    assert repomanager.PackageNameParser.determinePackageType("test.src.rpm") == repomanager.PackageType.Source
    assert repomanager.PackageNameParser.stripPackageExtension("test.rpm") == "test"
    assert repomanager.PackageNameParser.stripPackageExtension("test.src.rpm") == "test"
    assert repomanager.PackageNameParser.stripPackageExtension("test.src.rpm") == "test"

    assert repomanager.PackageNameParser.determinePackageArchitecture("test.src.rpm") == repomanager.PackageArchitecture.NotApplicable
    assert repomanager.PackageNameParser.determinePackageArchitecture("test.x86_64.rpm") == repomanager.PackageArchitecture.X86_64
    assert repomanager.PackageNameParser.determinePackageArchitecture("test.i386.rpm") == repomanager.PackageArchitecture.X86
    assert repomanager.PackageNameParser.stripPackageExtensionAndArchitecture("test.x86_64.rpm") == "test"
    assert repomanager.PackageNameParser.stripPackageExtensionAndArchitecture("test.src.rpm") == "test"
    assert repomanager.PackageNameParser.determinePackagePlatform("test.el6.src.rpm") == "el6"
    assert repomanager.PackageNameParser.determinePackagePlatform("test.el6.x86_64.rpm") == "el6"
    assert repomanager.PackageNameParser.determinePackagePlatform("test.el6.cern.x86_64.rpm") == "el6"
    assert repomanager.PackageNameParser.determinePackagePlatform("test.el6.cern.src.rpm") == "el6"
    assert repomanager.PackageNameParser.determinePackageNameWithVersionAndRelease("test.el6.cern.src.rpm") == "test"
    assert repomanager.PackageNameParser.determinePackageNameWithVersionAndRelease("test.test2.el6.cern.src.rpm") == "test.test2"

def test_PackageDescriptor():
    descr = repomanager.PackageDescriptor("/dir1/dir2/pkg1-1.2.3-134.el7.src.rpm")
    assert descr.getFullPath() == "/dir1/dir2/pkg1-1.2.3-134.el7.src.rpm"
    assert descr.getFilename() == "pkg1-1.2.3-134.el7.src.rpm"
    assert descr.getDirectory() == "/dir1/dir2"
    assert descr.getPackageType() == repomanager.PackageType.Source
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.NotApplicable
    assert descr.getPackagePlatform() == "el7"
    assert descr.getPackageNameWithVersionAndRelease() == "pkg1-1.2.3-134"
    assert descr.getPackageVersion() == "1.2.3"
    assert descr.getPackageRelease() == "134"
    assert descr.getPackageName() == "pkg1"

    descr = repomanager.PackageDescriptor("/dir1/dir2/pkg1-3.4.5-999.el6.cern.x86_64.rpm")
    assert descr.getFullPath() == "/dir1/dir2/pkg1-3.4.5-999.el6.cern.x86_64.rpm"
    assert descr.getFilename() == "pkg1-3.4.5-999.el6.cern.x86_64.rpm"
    assert descr.getDirectory() == "/dir1/dir2"
    assert descr.getPackageType() == repomanager.PackageType.Binary
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.X86_64
    assert descr.getPackagePlatform() == "el6"
    assert descr.getPackageNameWithVersionAndRelease() == "pkg1-3.4.5-999"
    assert descr.getPackageVersion() == "3.4.5"
    assert descr.getPackageRelease() == "999"
    assert descr.getPackageName() == "pkg1"

def test_QDBPackageParsing():
    descr = repomanager.PackageDescriptor("/tmp/quarkdb-0.2.9.15.b7bb108-1.el7.cern.x86_64.rpm")
    assert descr.getPackageType() == repomanager.PackageType.Binary
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.X86_64
    assert descr.getPackagePlatform() == "el7"
    assert descr.getPackageNameWithVersionAndRelease() == "quarkdb-0.2.9.15.b7bb108-1"
    assert descr.getPackageVersion() == "0.2.9.15.b7bb108"
    assert descr.getPackageRelease() == "1"
    assert descr.getPackageName() == "quarkdb"

def test_EOSPackageParsing():
    descr = repomanager.PackageDescriptor("/tmp/eos-server-4.3.5-20180727161839gitb233b4e.el7.cern.x86_64.rpm")
    assert descr.getPackageType() == repomanager.PackageType.Binary
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.X86_64
    assert descr.getPackagePlatform() == "el7"
    assert descr.getPackageNameWithVersionAndRelease() == "eos-server-4.3.5-20180727161839gitb233b4e"
    assert descr.getPackageVersion() == "4.3.5"
    assert descr.getPackageRelease() == "20180727161839gitb233b4e"
    assert descr.getPackageName() == "eos-server"

def test_GridHammerPackageParsing():
    descr = repomanager.PackageDescriptor("/tmp/grid-hammer-0.0.1.82.d75ee5e-1.el7.cern.x86_64.rpm")
    assert descr.getPackageType() == repomanager.PackageType.Binary
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.X86_64
    assert descr.getPackagePlatform() == "el7"
    assert descr.getPackageNameWithVersionAndRelease() == "grid-hammer-0.0.1.82.d75ee5e-1"
    assert descr.getPackageVersion() == "0.0.1.82.d75ee5e"
    assert descr.getPackageRelease() == "1"
    assert descr.getPackageName() == "grid-hammer"

def test_DavixPackageParsing():
    descr = repomanager.PackageDescriptor("/tmp/davix-doc-0.6.7.70.2c1ca73-1.fc27.noarch.rpm")
    assert descr.getPackageType() == repomanager.PackageType.NoArch
    assert descr.getPackageArchitecture() == repomanager.PackageArchitecture.NotApplicable
    assert descr.getPackagePlatform() == "fc27"
    assert descr.getPackageNameWithVersionAndRelease() == "davix-doc-0.6.7.70.2c1ca73-1"
    assert descr.getPackageVersion() == "0.6.7.70.2c1ca73"
    assert descr.getPackageRelease() == "1"
    assert descr.getPackageName() == "davix-doc"
